# IMDb_PySpark
This is a Python project that searches the IMDb public database using custom methods and writes the results into .csv files. Datasets can be found here: https://www.imdb.com/interfaces/

## Searching Methods
* Get all titles of series/movies etc. that are available in Ukrainian by sorting dataframe.
* Get the list of people’s names, who were born in the 19th century.
* Get titles of all movies that last more than 2 hours.
* Get names of people, corresponding movies/series and characters they played in those films.
* Get information about how many adult movies/series etc. there are per region. And then gets the top 100 of them from the region with the biggest count to the region with the smallest one.
* Get information about how many episodes in each TV Series. Get the top 50 of them starting from the TV Series with the biggest quantity of episodes.
* Get 10 titles of the most popular movies/series etc. by each decade. 
* Get 10 titles of the most popular movies/series etc. by each genre. 
