import sys

sys.path.append('D:\Python\PySpark\IMDb_Project')

import unittest
from pyspark import SparkConf
from pyspark.sql import SparkSession
from IMDb_Code.IMDb import *
import pyspark.sql.types as t

class TestIMDb(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.spark_session = (SparkSession.builder
                             .master("local")
                             .appName("task app")
                             .config(conf=SparkConf())
                             .getOrCreate())

    @classmethod
    def tearDownClass(cls):
        cls.spark_session.stop()


    def test_column_count(self):
        for i in range(len(dataframe_list)):
            dataframe_column_count = len(dataframe_list[i].columns)
            tsv_column_count = len(read_from_tsv(path_list[i], schema_list[i]).columns)
            self.assertEqual(dataframe_column_count, tsv_column_count)


    def test_create_dataframe(self):
        dataframe_list_len = len(dataframe_list)
        for i in range(dataframe_list_len):
            test_dataframe = self.spark_session.read.csv(path_list[i], sep=r'\t',nullValue='\\N', header=True, schema = schema_list[i])

            for j in range(len(bool_columns_list)):
                if bool_columns_list[j] in test_dataframe.columns:
                    test_dataframe = test_dataframe.withColumn(bool_columns_list[j], f.col(bool_columns_list[j]).cast(t.BooleanType()))


            result = test_dataframe.schema == dataframe_list[i].schema

            self.assertTrue(result)


    def test_avaible_in_ukrain(self):
        input_data = [('1', 1, 'Test_UA_1', 'UA', 'null', 'null', 'null', 1),
                      ('2', 1, 'Test_UA_2', 'US', 'null', 'null', 'null', 1),
                      ('3', 1, 'Test_UA_3', 'UA', 'null', 'null', 'null', 1)]
        input_df = self.spark_session.createDataFrame(input_data, schema_list[0])

        expected_schema = t.StructType([t.StructField(title_id, t.StringType(),  True),
                                        t.StructField(title,    t.StringType(),  True),
                                        t.StructField(region,   t.StringType(),  True),
        ])

        transformed_df = availbel_in_ukrain(input_df)

        result = transformed_df.schema == expected_schema
        self.assertTrue(result)

        result = transformed_df.where(f.col(region) != 'UA').count() == 0
        self.assertTrue(result)

        result = transformed_df.where(f.col(region) == 'UA').count() == 2
        self.assertTrue(result)


    def test_born_in_19th(self):
        input_data = [('1', 'Test_Name_1', 1850, 1900, 'test', 'test'),
                      ('2', 'Test_Name_2', 1902, 1980, 'test', 'test'),
                      ('3', 'Test_Name_3', 1801, 1870, 'test', 'test')]
        input_df = self.spark_session.createDataFrame(input_data, schema_list[6])

        expected_schema = t.StructType([t.StructField(person_id,    t.StringType(),  True),
                                        t.StructField(person_name,  t.StringType(),  True),
                                        t.StructField(birth_year,   t.IntegerType(),  True),
                                        t.StructField(death_year,   t.IntegerType(),  True),
        ])

        transformed_df = born_in_19th(input_df)

        result = transformed_df.schema == expected_schema
        self.assertTrue(result)

        result = transformed_df.where((f.col(birth_year) >= 1900) & (f.col(birth_year) <= 1801)).count() == 0
        self.assertTrue(result)

        result = transformed_df.where((f.col(birth_year) <= 1900) & (f.col(birth_year) >= 1801)).count() == 2
        self.assertTrue(result)


    def test_movies_2_hours_or_more(self):
        input_data = [('1', 'movie', 'Test_Name_1', 'Test_1', 0, 1850, 1900, 121, 'test'),
                      ('2', 'movie', 'Test_Name_2', 'Test_2', 0, 1850, 1900, 90, 'test'),
                      ('3', 'short', 'Test_Name_3', 'Test_3', 0, 1850, 1900, 130, 'test')]
        input_df = self.spark_session.createDataFrame(input_data, schema_list[1])

        expected_schema = t.StructType([t.StructField(title_id,         t.StringType(),  True),
                                        t.StructField(title_type,       t.StringType(),  True),
                                        t.StructField(primary_title,    t.StringType(),  True),
                                        t.StructField(runtime_minutes, t.IntegerType(),  True),
        ])

        transformed_df = movies_more_then_2_hours(input_df)

        result = transformed_df.schema == expected_schema
        self.assertTrue(result)

        result = transformed_df.where(f.col(runtime_minutes) <= 120).count() == 0
        self.assertTrue(result)

        result = transformed_df.where(f.col(runtime_minutes) >= 120).count() == 1
        self.assertTrue(result)


    def test_actors_and_roles(self):
        input_data1 = [('1', 1, '1', 'actor', 'null', '[test1]'),
                       ('2', 2, '2', 'director ','null', '[test2]'),
                       ('3', 3, '1', 'actor', 'null', '[test3]')]

        input_data2 = [('1', 'Test_Name_1', 1850, 1900, 'test', 'test'),
                      ('2', 'Test_Name_2', 1902, 1980, 'test', 'test')]
                      
        input_data3 = [('1', 'movie', 'Test_Name_1', 'Test_1', 0, 1850, 1900, 121, 'test'),
                       ('2', 'movie', 'Test_Name_2', 'Test_2', 0, 1850, 1900, 90, 'test'),
                       ('3', 'short', 'Test_Name_3', 'Test_3', 0, 1850, 1900, 130, 'test')]             
        input_df1 = self.spark_session.createDataFrame(input_data1, schema_list[4])
        input_df2 = self.spark_session.createDataFrame(input_data2, schema_list[6])
        input_df3 = self.spark_session.createDataFrame(input_data3, schema_list[1])

        expected_schema = t.StructType([t.StructField(person_id,   t.StringType(),  True),
                                        t.StructField(person_name, t.StringType(),  True),
                                        t.StructField(job_category, t.StringType(), True),
                                        t.StructField(characters,  t.StringType(),  False),
        ])

        transformed_df = actors_and_roles(input_df1, input_df2, input_df3)

        result = transformed_df.schema == expected_schema
        self.assertTrue(result)

        result = transformed_df.where(f.col(job_category) != 'actor').count() == 0
        self.assertTrue(result)

        result = transformed_df.where(f.col(job_category) == 'actor').count() == 1
        self.assertTrue(result)


    def test_top_100_adult_titles_in_regions(self):
        input_data1 = [('1', 'movie', 'Test_Name_1', 'Test_1', True, 1850, 1900, 121, 'test'),
                       ('2', 'movie', 'Test_Name_2', 'Test_2', True, 1850, 1900, 90, 'test'),
                       ('3', 'short', 'Test_Name_3', 'Test_3', True, 1903, 1903, 130, 'test'),
                       ('4', 'short', 'Test_Name_4', 'Test_4', False, 1850, 1900, 130, 'test'),
                       ('5', 'short', 'Test_Name_5', 'Test_5', False, 1850, 1900, 130, 'test')]  

        input_schema1 =t.StructType([t.StructField(title_id,           t.StringType(),  True),
                                     t.StructField(title_type,         t.StringType(),  True),
                                     t.StructField(primary_title,      t.StringType(),  True),
                                     t.StructField(original_title,     t.StringType(),  True),
                                     t.StructField(is_adult,           t.BooleanType(),True),
                                     t.StructField(start_year,         t.IntegerType(), True),
                                     t.StructField(end_year,           t.StringType(),  True),
                                     t.StructField(runtime_minutes,   t.IntegerType(), True),
                                     t.StructField(geners,             t.StringType(),  True),
])        

        input_data2 = [('1', 1, 'Test_1', 'UA', 'null', 'null', 'null', 1),
                       ('2', 1, 'Test_2', 'US', 'null', 'null', 'null', 1),
                       ('3', 1, 'Test_3', 'UA', 'null', 'null', 'null', 1),
                       ('4', 1, 'Test_3', 'DE', 'null', 'null', 'null', 1),
                       ('5', 1, 'Test_3', 'UA', 'null', 'null', 'null', 1)]

        input_data3 = [('1', 10.0, 150),
                       ('2', 5.5, 10),
                       ('3', 8.3, 110),
                       ('4', 6.6, 210),
                       ('5', 4.8, 21)]
        input_df1 = self.spark_session.createDataFrame(input_data1, input_schema1)
        input_df2 = self.spark_session.createDataFrame(input_data2, schema_list[0])
        input_df3 = self.spark_session.createDataFrame(input_data3, schema_list[5])

        expected_schema = t.StructType([t.StructField(title_id,       t.StringType(),  True),
                                        t.StructField(region,         t.StringType(),  True),
                                        t.StructField(title,          t.StringType(),  True),
                                        t.StructField(is_adult,       t.BooleanType(), True),
                                        t.StructField(count,          t.LongType(),    True),
                                        t.StructField(average_rating, t.DoubleType(),  True),
                                        t.StructField(top,            t.IntegerType(), False),
        ])

        transformed_df = top_100_adult_titles_in_regions(input_df1, input_df2, input_df3)

        result = transformed_df.schema == expected_schema
        self.assertTrue(result)

        result = transformed_df.where(f.col(region) == 'UA').count() == 2
        self.assertTrue(result)

        result = transformed_df.where(f.col(region) == 'US').count() == 1
        self.assertTrue(result)

        result = transformed_df.where(f.col(region) == 'DE').count() == 0
        self.assertTrue(result)


    def test_top_50_tvSeries(self):
        input_data1 = [('1', '', 'Test_Name_1', 'Test_1', 1, 1850, 1900, 121, 'test'),
                       ('2', '', 'Test_Name_2', 'Test_2', 1, 1850, 1900, 90, 'test'),
                       ('3', '', 'Test_Name_3', 'Test_3', 1, 1903, 1903, 130, 'test'),
                       ('4', '', 'Test_Name_4', 'Test_4', 0, 1850, 1900, 130, 'test'),
                       ('5', '', 'Test_Name_5', 'Test_5', 0, 1850, 1900, 130, 'test')] 

        input_data2 = [('1','6', 1, 1),
                       ('2','6', 1, 2),
                       ('3','6', 1, 3),
                       ('4','7', 1, 1),
                       ('5','7', 1, 2)]

        input_df1 = self.spark_session.createDataFrame(input_data1, schema_list[1])
        input_df2 = self.spark_session.createDataFrame(input_data2, schema_list[3])

        expected_schema = t.StructType([t.StructField(parent_series_id, t.StringType(),  True),
                                        t.StructField(count,            t.LongType(),    False),
                                        t.StructField(top,              t.IntegerType(), False),
        ])

        transformed_df = top_50_tvSeries(input_df1, input_df2)

        result = transformed_df.schema == expected_schema
        self.assertTrue(result)

        result = transformed_df.count() == 2
        self.assertTrue(result)

    def test_top_10_titles_decade(self):
        input_data1 = [('1', '', 'Test_Name_1', 'Test_1', 1, 1850, 1850, 121, 'test'),
                       ('2', '', 'Test_Name_2', 'Test_2', 1, 1855, 1865, 90, 'test'),
                       ('3', '', 'Test_Name_3', 'Test_3', 1, 1903, 1903, 130, 'test'),
                       ('4', '', 'Test_Name_4', 'Test_4', 0, 1783, 1783, 130, 'test'),
                       ('5', '', 'Test_Name_5', 'Test_5', 0, 2010, 2010, 130, 'test')] 

        input_data2 = [('1', 10.0, 150),
                       ('2', 5.5, 10),
                       ('3', 8.3, 110),
                       ('4', 6.6, 210),
                       ('5', 4.8, 21)]

        input_df1 = self.spark_session.createDataFrame(input_data1, schema_list[1])
        input_df2 = self.spark_session.createDataFrame(input_data2, schema_list[5])

        expected_schema = t.StructType([t.StructField(title_id,       t.StringType(),  True),
                                        t.StructField(primary_title,  t.StringType(),  True),
                                        t.StructField(start_year,     t.IntegerType(), True),
                                        t.StructField(average_rating, t.DoubleType(),  True),
                                        t.StructField(decade,         t.IntegerType(),  True),
                                        t.StructField(top,            t.IntegerType(), False),
        ])
        
        transformed_df = top_10_titles_decade(input_df1, input_df2)

        result = transformed_df.schema == expected_schema
        self.assertTrue(result)

        result = transformed_df.where(f.col(decade) == 1850).count() == 2
        self.assertTrue(result)

        result = transformed_df.where(f.col(top) > 10).count() == 0
        self.assertTrue(result)


    def test_top_10_title_genre(self):
        input_data1 = [('1', '', 'Test_Name_1', 'Test_1', 1, 1850, 1850, 121, 'action'),
                       ('2', '', 'Test_Name_2', 'Test_2', 1, 1855, 1865, 90,  'action'),
                       ('3', '', 'Test_Name_3', 'Test_3', 1, 1903, 1903, 130, 'test'),
                       ('4', '', 'Test_Name_4', 'Test_4', 0, 1783, 1783, 130, 'adventure'),
                       ('5', '', 'Test_Name_5', 'Test_5', 0, 2010, 2010, 130, 'crime')] 

        input_data2 = [('1', 10.0, 150),
                       ('2', 5.5, 10),
                       ('3', 8.3, 110),
                       ('4', 6.6, 210),
                       ('5', 4.8, 21)]

        input_df1 = self.spark_session.createDataFrame(input_data1, schema_list[1])
        input_df2 = self.spark_session.createDataFrame(input_data2, schema_list[5])

        expected_schema = t.StructType([t.StructField(title_id,       t.StringType(),  True),
                                        t.StructField(primary_title,  t.StringType(),  True),
                                        t.StructField(geners,         t.StringType(),  True),
                                        t.StructField(average_rating, t.DoubleType(),  True),
                                        t.StructField(votes_number,   t.IntegerType(), True),
                                        t.StructField(gener,          t.StringType(),  False),
                                        t.StructField(top,            t.IntegerType(), False),
        ])

        transformed_df = top_10_title_genre(input_df1, input_df2)

        result = transformed_df.schema == expected_schema
        self.assertTrue(result)

        result = transformed_df.where(f.col(gener) == 'action').count() == 2
        self.assertTrue(result)

        result = top_10_genre_title.where(f.col(top) > 10).count() == 0
        self.assertTrue(result)

if __name__ == '__main__':
    unittest.main(argv=[''],verbosity=2, exit=False)
