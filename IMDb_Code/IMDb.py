from pyspark import SparkConf
from pyspark.sql import SparkSession, Window
import pyspark.sql.types as t
import pyspark.sql.functions as f
from pyspark.sql.functions import row_number

spark_session = (SparkSession.builder
                             .master("local")
                             .appName("task app")
                             .config(conf=SparkConf())
                             .getOrCreate())

title_akas_path = 'D:\Python\PySpark\IMDb_Project/title.akas.tsv\data.tsv'
title_basic_path = 'D:\Python\PySpark\IMDb_Project/title.basics.tsv\data.tsv'
title_crew_path = 'D:\Python\PySpark\IMDb_Project/title.crew.tsv\data.tsv'
title_episode_path = 'D:\Python\PySpark\IMDb_Project/title.episode.tsv\data.tsv'
title_principals_path = 'D:\Python\PySpark\IMDb_Project/title.principals.tsv\data.tsv'
title_ratings_path = 'D:\Python\PySpark\IMDb_Project/title.ratings.tsv\data.tsv'
name_basics_path = 'D:\Python\PySpark\IMDb_Project/name.basics.tsv\data.tsv'

"""string: Path variables that points to the data location."""

title_id = 'Title_ID'
ordering ='Ordering'
title = 'Title'
region = 'Region'
language = 'Language'
types = 'Types'
attributes = 'Attributes'
is_original_title = 'Is_Original_Title'

title_type = 'Title_Type'
primary_title = 'Primary_Title'
original_title = 'Original_Title'
is_adult = 'Is_Adult'
start_year = 'Start_Year'
end_year = 'End_Year'
runtime_minutes = 'runtime_minutes'
geners = 'Geners'

directors = 'Directors'
writers = 'Writers'

parent_series_id = 'Parent_Series_ID'
season_number ='Season_Number'
episode_number = 'Episode_Number'

person_id = 'Person_ID'
job_category = 'Job_Category'
job = 'Job'
characters = 'Characters'

average_rating = 'Average_Rating'
votes_number = 'Votes_Number'

person_name = 'Person_Name'
birth_year = 'Birth_Year'
death_year = 'Death_Year'
primary_profession = 'Primary_Profession'
know_for_title ='Known_For_Title'

"""string: Names of colums in dataframes. Later used in creating structers and in functions."""

title_akas_schema = t.StructType([      t.StructField(title_id,           t.StringType(),  True),
                                        t.StructField(ordering,           t.IntegerType(), True),
                                        t.StructField(title,              t.StringType(),  True),
                                        t.StructField(region,             t.StringType(),  True),
                                        t.StructField(language,           t.StringType(),  True),
                                        t.StructField(types,              t.StringType(),  True),
                                        t.StructField(attributes,         t.StringType(),  True),
                                        t.StructField(is_original_title,  t.IntegerType(), True),
])

"""Contains:

title_id (string) - a tconst, an alphanumeric unique identifier of the title.
ordering (integer) – a number to uniquely identify rows for a given titleId.
title (string) – the localized title.
region (string) - the region for this version of the title.
language (string) - the language of the title.
types (string) - Enumerated set of attributes for this alternative title. One or more of the following: "alternative", "dvd", "festival", "tv", "video", "working", "original", "imdbDisplay". New values may be added in the future without warning.
attributes (string) - Additional terms to describe this alternative title, not enumerated.
is_original_title (boolean) – 0: not original title; 1: original title."""

title_basic_schema = t.StructType([     t.StructField(title_id,           t.StringType(),  True),
                                        t.StructField(title_type,         t.StringType(),  True),
                                        t.StructField(primary_title,      t.StringType(),  True),
                                        t.StructField(original_title,     t.StringType(),  True),
                                        t.StructField(is_adult,           t.IntegerType(),True),
                                        t.StructField(start_year,         t.IntegerType(), True),
                                        t.StructField(end_year,           t.StringType(),  True),
                                        t.StructField(runtime_minutes,    t.IntegerType(), True),
                                        t.StructField(geners,             t.StringType(),  True),
])

"""Contains:

title_id (string) - alphanumeric unique identifier of the title.
title_type (string) – the type/format of the title (e.g. movie, short, tvseries, tvepisode, video, etc).
primary_title (string) – the more popular title / the title used by the filmmakers on promotional materials at the point of release.
original_title (string) - original title, in the original language.
is_adult (boolean) - 0: non-adult title; 1: adult title.
start_year (integer) – represents the release year of a title. In the case of TV Series, it is the series start year.
end_year (integer) – TV Series end year. ‘\N’ for all other title types.
runtime_minutes – primary runtime of the title, in minutes.
genres (string) – includes up to three genres associated with the title."""

title_crew_schema = t.StructType([      t.StructField(title_id,           t.StringType(),  True),
                                        t.StructField(directors,          t.StringType(),  True),
                                        t.StructField(writers,            t.StringType(),  True),
])

"""Contains:

title_id (string) - alphanumeric unique identifier of the title.
directors (string) - director(s) of the given title.
writers (string) – writer(s) of the given title."""

title_episode_schema = t.StructType([   t.StructField(title_id,           t.StringType(),  True),
                                        t.StructField(parent_series_id,   t.StringType(),  True),
                                        t.StructField(season_number,      t.IntegerType(), True),
                                        t.StructField(episode_number,     t.IntegerType(), True),
])

"""Contains:

title_id (string) - alphanumeric identifier of episode.
parent_series_id (string) - alphanumeric identifier of the parent TV Series.
season_number (integer) – season number the episode belongs to.
episode_number (integer) – episode number of the tconst in the TV series."""

title_principals_schema = t.StructType([t.StructField(title_id,           t.StringType(),  True),
                                        t.StructField(ordering,           t.IntegerType(), True),
                                        t.StructField(person_id,          t.StringType(),  True),
                                        t.StructField(job_category,       t.StringType(),  True),
                                        t.StructField(job,                t.StringType(),  True),
                                        t.StructField(characters,         t.StringType(),  True),
])

"""Contains:

title_id (string) - alphanumeric unique identifier of the title.
ordering (integer) – a number to uniquely identify rows for a given titleId.
person_id (string) - alphanumeric unique identifier of the name/person.
job_category (string) - the category of job that person was in.
job (string) - the specific job title if applicable, else '\N'.
characters (string) - the name of the character played if applicable, else '\N'."""

title_ratings_schema = t.StructType([   t.StructField(title_id,           t.StringType(),  True),
                                        t.StructField(average_rating,     t.DoubleType(),  True),
                                        t.StructField(votes_number,       t.IntegerType(), True),
])

"""Contains:

title_id (string) - alphanumeric unique identifier of the title.
average_rating – weighted average of all the individual user ratings.
num_votes - number of votes the title has received."""

name_basics_schema = t.StructType([     t.StructField(person_id,          t.StringType(),  True),
                                        t.StructField(person_name,        t.StringType(),  True),
                                        t.StructField(birth_year,         t.IntegerType(), True),
                                        t.StructField(death_year,         t.IntegerType(), True),
                                        t.StructField(primary_profession, t.StringType(),  True),
                                        t.StructField(know_for_title,     t.StringType(),  True),
])

"""Contains:

person_id (string) - alphanumeric unique identifier of the name/person.
primary_name (string)– name by which the person is most often credited.
birth_year – in YYYY format.
death_year – in YYYY format if applicable, else '\N'.
primary_profession (array of strings)– the top-3 professions of the person.
known_for_title (array of tconsts) – titles the person is known for."""

path_list = [title_akas_path, title_basic_path, title_crew_path, title_episode_path,
               title_principals_path, title_ratings_path, name_basics_path]

"""list: list of paths for every dataframe. Used in functions to create dataframes and tests."""

schema_list = [title_akas_schema, title_basic_schema, title_crew_schema, title_episode_schema, 
               title_principals_schema, title_ratings_schema, name_basics_schema]

"""list: list of schemas for every dataframe. Used in functions to create dataframes and tests."""

bool_columns_list = [is_original_title, is_adult]

"""list: list of columns that should be casted into boolean."""


def read_from_tsv(path, schema):
    """Reads data from tsv file and returns it as dataframe applied with schema. 
    
    Args:
        path(string): path to the data file from which data would be read.
        schema(StructType): schema that would be applied on read data.

    Returns:
        DataFrame: data from tsv with applied schema.

    """
    return spark_session.read.csv(path, sep=r'\t',nullValue='\\N', header=True, schema = schema)

def correct_boolean_values(dataframe):
    """Correctly transform booleand values in dataframe. Takes columns to correct from bool_columns_list.

    Args: 
        dataframe(DataFrame): Dataframe that needs it values to be corrected.
    
    Returns:
        DataFrame: Dataframe with corrected values.

    """
    for i in range(len(bool_columns_list)):
            if bool_columns_list[i] in dataframe.columns:
                dataframe = dataframe.withColumn(bool_columns_list[i], f.col(bool_columns_list[i]).cast(t.BooleanType()))
    return dataframe


def create_dataframe(path, schema):
    """Creates dataframe with correted values.

    Args:
        path(string): path to the data file from which data would be read.
        schema(StructType): schema that would be applied on read data.
    
    Returns:
       DataFrame: data from tsv with applied schema and corrected values. 
    
    """
    dataframe = read_from_tsv(path, schema)

    dataframe = correct_boolean_values(dataframe)
     
    return dataframe
                                    
title_akas_df =  create_dataframe( title_akas_path,  title_akas_schema)

"""title_akas_df - Contains information for titles regions."""

title_basic_df =  create_dataframe( title_basic_path,  title_basic_schema) 

"""title_basics_df - Contains information for titles."""

title_crew_df =  create_dataframe( title_crew_path,  title_crew_schema) 

"""title_crew_df – Contains the director and writer information for all the titles in IMDb."""

title_episode_df =  create_dataframe( title_episode_path,  title_episode_schema) 

"""title_episode_df – Contains the tv episode information."""

title_principals_df =  create_dataframe( title_principals_path,  title_principals_schema) 

"""title_principals_df – Contains the principal cast/crew for titles."""

title_ratings_df =  create_dataframe( title_ratings_path,  title_ratings_schema) 

"""title_ratings_df – Contains the IMDb rating and votes information for titles."""

name_basics_df =  create_dataframe( name_basics_path,  name_basics_schema) 

"""name_basics_df – Contains the following information for names."""

dataframe_list=[title_akas_df, title_basic_df, title_crew_df, title_episode_df, title_principals_df, title_ratings_df, name_basics_df]

"""list: contains all created dataframes"""

def show_dfs():
    """Shows all created and listed dataframes in dataframe_list"""
    for i in range(len(dataframe_list)):
        dataframe_list[i].show()



def availbel_in_ukrain(titles_region_dataframe):
    """Gets all titles of series/movies etc. that are available in Ukrainian by sorting dataframe.
    
    Args:
        titles_region_dataframe(DataFrame): dataframe that contains titles and regions such as title_akas_df.

    Retruns:
        dataframe: datframe with selected titles availebl in Ukrain.

    """
    dataframe = (titles_region_dataframe.select(title_id, title, region).where(f.col(region) == 'UA'))
    return dataframe
availbel_ukrain = availbel_in_ukrain(title_akas_df)



def born_in_19th(names_dataframe):
    """Gets the list of people’s names, who were born in the 19th century.
    
    Args:
        names_dataframe(DataFrame): dataframe that contains names as name_basics_df.

    Retruns:
        dataframe: datframe of people’s names, who were born in the 19th century.

    """
    dataframe = (names_dataframe.where((f.col(birth_year) <= 1900) & (f.col(birth_year) >= 1801))
                               .drop(primary_profession, know_for_title))
    return dataframe
born_19th = born_in_19th(name_basics_df)



def movies_more_then_2_hours(titles_dataframe):
    """Gets titles of all movies that last more than 2 hours.
    
    Args:
        titles_dataframe(DataFrame): dataframe that contains titles such as title_basic_df.

    Retruns:
        dataframe: datframe with titles of all movies that last more than 2 hours.

    """
    dataframe = (titles_dataframe.select(title_id, title_type, primary_title, runtime_minutes)
                                 .where((f.col(title_type) == 'movie') & (f.col(runtime_minutes) > 120)))
    return dataframe
movies_2_hours_or_more = movies_more_then_2_hours(title_basic_df)



def actors_and_roles(principals_dataframe, names_dataframe, titles_dataframe):
    """Gets names of people, corresponding movies/series and characters they played in those films. 
    
    Args:
        principals_dataframe(DataFrame): dataframe that contains principal cast/crew for titles such as title_principals_df.
        names_dataframe(DataFrame): dataframe that contains names as name_basics_df.
        titles_dataframe(DataFrame): dataframe that contains titles such as title_basic_df.

    Retruns:
        dataframe: datframe with names of people, corresponding movies/series and characters they played in those films.

    """
    dataframe = principals_dataframe.where(f.col(job_category) == 'actor').drop(job, ordering)

    dataframe = (dataframe.join(names_dataframe.select(person_id, person_name), on=person_id, how='left'))

    dataframe = (dataframe.join(titles_dataframe.select(title_id, primary_title), on=title_id, how='left')
                          .drop(title_id))
                          
    dataframe = (dataframe.groupBy(person_id, person_name, job_category)
                          .agg(f.concat_ws(', ', f.collect_list(characters)).alias(characters))
                          .orderBy(person_id))
    return dataframe
actors_roles = actors_and_roles(title_principals_df, name_basics_df, title_basic_df)



top = 'top'
count = 'count'
def top_100_adult_titles_in_regions(titles_dataframe, titles_region_dataframe, ratings_dataframe):
    """Gets information about how many adult movies/series etc. there are per region. And then gets the top 100 of them from the region with the biggest count to the region with the smallest one.
    
    Args:
        titles_dataframe(DataFrame): dataframe that contains titles such as title_basic_df.
        titles_region_dataframe(DataFrame): dataframe that contains titles and regions such as title_akas_df.
        ratings_dataframe(DataFrame): dataframe that contains the IMDb rating and votes information for titles such as title_ratings_df.

    Retruns:
        dataframe: datframe with top 100 adults titles in every region, sorted by region with the biggest count to the region with the smallest one.

    """
    
    adult_dataframe = (titles_region_dataframe.select(title_id, title, region)
                       .join( titles_dataframe.select(title_id, is_adult), on=title_id, how='left'))

    adult_dataframe = adult_dataframe.where((f.col(is_adult)== True) & (f.col(region)!= 'null'))

    top_regions = adult_dataframe.groupBy(region).count().orderBy(count, ascending=False)

    adult_dataframe = adult_dataframe.join(top_regions, on=region, how='left')

    adult_dataframe = adult_dataframe.join(ratings_dataframe, on=title_id, how='left').drop(votes_number)

    windowAdult = Window.partitionBy(region).orderBy(f.col(count).desc(), f.col(average_rating).desc())

    adult_dataframe = adult_dataframe.withColumn(top, row_number().over(windowAdult))

    adult_dataframe = (adult_dataframe.orderBy( f.col(count).desc(), f.col(top).asc(), f.col(average_rating).desc())
                                      .where( f.col(top) <= 100))
    return adult_dataframe
top_100_adult_titles = top_100_adult_titles_in_regions(title_basic_df, title_akas_df, title_ratings_df)


def create_top_n(n, dataframe, window):
    """Creates dataframe with top n results 
    
    Args:
        n(integer): amount of entries in top.
        dataframe(Dataframe): dataframe from which a top should be created.
        window(Window): statement by which dataframe will be rated

    Retruns:
        dataframe: dataframe with a top and column 'top', that represents the position in it.

    """
    dataframe = dataframe.withColumn(top,row_number().over(window))
    return dataframe.where(f.col(top) <= n)


def top_50_tvSeries(titles_dataframe, episodes_dataframe):
    """Gets information about how many episodes in each TV Series. Get the top 50 of them starting from the TV Series with the biggest quantity of episodes.
    
    Args:
        titles_dataframe(DataFrame): dataframe that contains titles such as title_basic_df.
        episodes_dataframe(DataFrame): dataframe that contains the tv episode information such as title_episode_df.

    Retruns:
        dataframe: datframe with a top 50 tv series listed on IMDb.

    """
    tvSeries = (titles_dataframe.join(episodes_dataframe, on=title_id, how='left')
                              .drop(original_title, is_adult, runtime_minutes, geners))

    tvSeries = tvSeries.where(f.col(episode_number) != 0).orderBy(f.col(primary_title))

    tvSeries = tvSeries.groupBy(parent_series_id).count()

    windowSeries = Window.orderBy(f.col(count).desc())

    tvSeries = create_top_n(50, tvSeries, windowSeries)

    return tvSeries
top_50_series = top_50_tvSeries(title_basic_df,title_episode_df)



decade = 'Decade'
def top_10_titles_decade(titles_dataframe, ratings_dataframe):
    """Gets 10 titles of the most popular movies/series etc. by each decade. 
    
    Args:
        titles_dataframe(DataFrame): dataframe that contains titles such as title_basic_df.
        ratings_dataframe(DataFrame): dataframe that contains the IMDb rating and votes information for titles such as title_ratings_df.

    Retruns:
        dataframe: datframe with a top 10 titles in each decade.

    """
    dataframe = (titles_dataframe.select(title_id, primary_title, start_year)
                                 .join(ratings_dataframe, on=title_id, how='left')
                                 .drop(votes_number))

    dataframe = (dataframe.withColumn(decade, (f.floor(f.col(start_year)/10)*10).cast(t.IntegerType()))
                          .where((f.col(start_year) != 0) & (f.col(average_rating) != 0)))

    windowDecade = Window.partitionBy(decade).orderBy(f.col(average_rating).desc(),f.col(primary_title))

    dataframe = create_top_n(10, dataframe, windowDecade)

    return dataframe

top_10_decade = top_10_titles_decade(title_basic_df, title_ratings_df)



gener = 'Top_Gener'
def top_10_title_genre(titles_dataframe, ratings_dataframe):
    """Gets 10 titles of the most popular movies/series etc. by each genre. 
    
    Args:
        titles_dataframe(DataFrame): dataframe that contains titles such as title_basic_df.
        ratings_dataframe(DataFrame): dataframe that contains the IMDb rating and votes information for titles such as title_ratings_df.

    Retruns:
        dataframe: datframe with a top 10 titles in each gener.

    """
    dataframe = (titles_dataframe.select(title_id, primary_title, geners)
                                 .join(ratings_dataframe, on=title_id, how='left'))

    dataframe = dataframe.where((f.col(geners) != 'null') & (f.col(average_rating) != 0))


    dataframe = dataframe.withColumn(gener, f.explode(f.split(f.col(geners), ',')))

    windowGener = Window.partitionBy(gener).orderBy(f.col(average_rating).desc(),f.col(votes_number).desc(), f.col(primary_title))

    dataframe = create_top_n(10, dataframe, windowGener)

    return dataframe
top_10_genre_title = top_10_title_genre(title_basic_df, title_ratings_df)



def save_dfs():
    """Saves all new dataframes, that was created from functions in to the corresponding folder."""

    path_to_save = 'Save_Data'
    availbel_ukrain.write.csv(path_to_save + '/availbel in ukrain', header=True, mode='overwrite')
    
    born_19th.write.csv(path_to_save + '/born in 19th century', header=True, mode='overwrite')

    movies_2_hours_or_more.write.csv(path_to_save + '/movies more then 2 hours', header=True, mode='overwrite')

    actors_roles.write.csv(path_to_save + '/actors and roles', header=True, mode='overwrite')

    top_100_adult_titles.write.csv(path_to_save + '/top 100 adult titles in regions', header=True, mode='overwrite')

    top_50_series.write.csv(path_to_save + '/top 50 series', header=True, mode='overwrite')

    top_10_decade.write.csv(path_to_save + '/top 10 titles every decade', header=True, mode='overwrite')

    top_10_genre_title.write.csv(path_to_save + '/top 10 titles in every genres', header=True, mode='overwrite')
